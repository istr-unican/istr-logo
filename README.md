# ISTR logo

Logo del grupo actualizado a formato vectorial y dimensiones cuadradas. Desarrollado en [Processing](https://processing.org/).

Los royalties siguen perteneciendo a [Alejandro Pérez](https://www.youtube.com/user/akilesss/videos).
