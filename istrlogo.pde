import processing.pdf.*;
import processing.svg.*;

void setup() {
  hint(ENABLE_STROKE_PURE);
  size(1000, 1000, PDF, "istrlogo.pdf");
  beginRecord(SVG, "istrlogo.svg");
}

void draw() {

  boolean coloured = true;
  boolean printGuidelines = false;

  int fontSize = 250;
  PFont font = createFont("Hack Bold", fontSize, true); // 16 point, anti-aliasing on
  textFont(font);

  // variables
  int center = width / 2;
  float ratio = 0.9;
  float diam = width * ratio;
  float radius = diam / 2;

  // background
  background(255);
  noStroke();
  color unicanColor = color(0, 103, 113);


  // right color
  if (coloured) {
    fill(unicanColor);
  } else {
    fill(150);
  }
  rect(width/2, 0, width, width);

  // clock ticks
  int smallThickness = 12;
  int bigThickness = 20;
  float smallTickCirc = width * ratio * 0.825 / 2;
  float bigTickCirc = width * ratio * 0.91 / 2;

  strokeCap(SQUARE);
  if (coloured) {
    stroke(255);
  } else {
    stroke(0);
  }

  strokeWeight(20);
  for (int angle = 270; angle <= 450; angle += 6) {
    float innerRadius;
    if (angle % 5 == 0) {
      strokeWeight(bigThickness);
      innerRadius = smallTickCirc;
    } else {
      strokeWeight(smallThickness);
      innerRadius = bigTickCirc;
    }
    float angleRad = radians(angle);
    println(angleRad);
    line(center + innerRadius * cos(angleRad), center - innerRadius * sin(angleRad),
         center + radius * cos(angleRad), center - radius * sin(angleRad));
  }

  // left arrow line
  if (coloured) {
    stroke(unicanColor);
  } else {
    stroke(0);
  }
  noFill();
  strokeWeight(bigThickness);

  float removedAngleArrow = 0.15;
  float removedAngleDiamond = 0.15;
  arc(center, center,
      bigTickCirc * 2, bigTickCirc * 2,
      HALF_PI + removedAngleDiamond, HALF_PI + PI - removedAngleArrow);


  if (coloured) {
    fill(unicanColor);
  } else {
    fill(0);
  }

  // left arrow diamond
  float diamondWidth = (bigTickCirc - smallTickCirc) * 1.75;
  float diamondLength = 1.5 * diamondWidth;

  float diamondCenterX = center + bigTickCirc * cos(HALF_PI + removedAngleDiamond / 2);
  float diamondCenterY = center - bigTickCirc * -sin(HALF_PI + removedAngleDiamond / 2);

  strokeWeight(2);
  pushMatrix();

  translate(diamondCenterX, diamondCenterY);
  rotate(radians(10));

  rectMode(CENTER);
  quad(0, 0,
       -diamondLength/2, diamondWidth/2,
       -diamondLength, 0,
       -diamondLength/2, -diamondWidth/2);
  popMatrix();



  // left arrow head
  float arrowWidth = (bigTickCirc - smallTickCirc) * 1.5;
  float arrowLength = 2 * diamondWidth;

  float arrowPointX = center - bigTickCirc * - cos(HALF_PI + removedAngleDiamond / 2);
  float arrowPointY = center + bigTickCirc * - sin(HALF_PI + removedAngleDiamond / 2);

  pushMatrix();

  translate(arrowPointX, arrowPointY);
  rotate(radians(-9));

  quad(0, 0,
       -arrowLength/2, arrowWidth/2,
       -arrowLength/3, 0,
       -arrowLength/2, -arrowWidth/2);

  popMatrix();


  // letters
  text("IS", center - fontSize*1.25, center + fontSize*0.37);

  if (coloured) {
    fill(255);
  }
  text("TR", center + fontSize*0.11, center + fontSize*0.37);

  // grid guidelines
  if (printGuidelines) {
    stroke(0);
    strokeWeight(1);
    noFill();
    ellipse(center, center, diam, diam);
    ellipse(center, center, smallTickCirc * 2, smallTickCirc * 2);
    ellipse(center, center, bigTickCirc * 2, bigTickCirc * 2);
    line(center, 0, center, width);
    line(0, center, width, center);
    float downY = center + (fontSize / 2) * 0.8;
    float upY = center - (fontSize / 2) * 0.8;
    line(0, downY, width, downY);
    line(0, upY, width, upY);
    float rightX = center + fontSize * 1.2;
    float leftX = center - fontSize * 1.2;
    line(rightX, 0, rightX, width);
    line(leftX, 0, leftX, width);
  }
  
  // export to SVG and png as well
  endRecord();
  
  // Exit the program
  println("Finished.");
  exit();
}
